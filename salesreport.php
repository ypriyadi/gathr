<!DOCTYPE html>
<html>
<head>
	<title>Gathr - Connected faster and simply</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<style type="text/css">
		.navbar {
			margin-top: 15px;
		}
		.navbar-nav { float: right; }
	</style>
</head>
<body>
	<div class="main_wrap">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="./salesreport"><img src="./img/logo.png"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="./salesreport
							">Dashboard <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="./myproducts">My products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Trending / Ideas</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">My account</a>
						</li>
					</ul>
					<div class="form-inline my-2 my-lg-0">
						<a href="./upload" class="btn btn-primary">Upload new product</a>
					</div>
				</div>
			</nav>
			<div class="content" style="margin-top: 15px;">
				<div class="row">
					<div class="col-md-2 offset-md-10">
						<div class="form-group">
							<div class="form-group">
								<select class="form-control" id="exampleFormControlSelect1">
									<option>Weekly</option>
									<option>Monthly</option>
									<option>All</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h2 class="text-center">Revenue</h2>
						<img src="img/chart_weekly_revenue.png">
						<br>
						<br>
						<br>
						<h2 class="text-center">Sales</h2>
						<img src="img/chart_weekly_revenue.png">
						<br>
						<br>
						<br>
						<h2 class="text-center">Traffic</h2>
						<img src="img/chart_weekly_revenue.png">
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>