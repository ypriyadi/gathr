<!DOCTYPE html>
<html>
<head>
	<title>Gathr - Connected faster and simply</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<style type="text/css">
		
	</style>
</head>
<body>
	<div class="main_wrap">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="./salesreport"><img src="./img/logo.png"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="./salesreport
							">Dashboard <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="./myproducts">My products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Trending / Ideas</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">My account</a>
						</li>
					</ul>
				</div>
			</nav>
			<h1 style="margin-top: 30px;">Upload new product</h1>
			<div class="content" style="margin-top: 15px">
				<div class="form-group">
					<div class="form-group">
						<label for="exampleFormControlSelect1">Select product Type</label>
						<select class="form-control product-type" id="productType">
							<option value="1">Graphic</option>
							<option value="1">Template</option>
							<option value="1">Font</option>
							<option value="2">Photos</option>
							<option value="2">Footage</option>
							<option value="2">Audio</option>
						</select>
					</div>
					<div class="form-group">
						<label for="exampleFormControlFile1">Your product preview / thumbnail</label>
						<input type="file" class="form-control-file" id="exampleFormControlFile1">
					</div>
					<a href="#" class="btn btn-primary uploadBtn" style="width: 100%;">Upload</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>