<!DOCTYPE html>
<html>
<head>
	<title>Gathr - Connected faster and simply</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<style type="text/css">
		.navbar {
			margin-top: 15px;
		}
		.navbar-nav { float: right; }
		.tab-products {
			width: 100%;
			margin: 0;
			padding: 0;
			margin-top: 20px;
		}
		.tab-products > li {
			list-style: none;
			border-top: 1px solid rgba(0,0,0,.1);
		}
		.tab-products > li:hover, .tab-products > li.active {
			background-color: #F1F4F5;
		}
		.tab-products > li > a {
			padding: 15px 20px;
			display: block;
			text-decoration: none;
			color: lightgray;
		}
		.tab-products > li.active > a,
		.tab-products > li:hover > a {
			color: #64B5F6;
		}
		.tab-contents {
			border-left: 1px solid rgba(0,0,0,.1);
		}
		.tab-content-item img {
			max-width: 100%;
		}
		.tab-content-item {
			display: none;
		}
		.tab-content-item.active { display: block; }
	</style>
</head>
<body>
	<div class="main_wrap">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="./salesreport"><img src="./img/logo.png"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="./salesreport
							">Dashboard <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="./myproducts">My products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Trending / Ideas</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">My account</a>
						</li>
					</ul>
					<div class="form-inline my-2 my-lg-0">
						<a href="./upload" class="btn btn-primary">Upload new product</a>
					</div>
				</div>
			</nav>
			<div class="content" style="margin-top: 15px;">
				<h2 style="margin-bottom: 30px;">My products</h2>
				<div class="row">
					<div class="col-md-4">
						<ul class="tab-products">
							<li class="active"><a href="#itemOne">The Sea & Friends Collection</a></li>
							<li><a href="#itemTwo">Sun & Fun Summer Beach</a></li>
							<li><a href="#itemThree">Forest Discoverer</a></li>
						</ul>
					</div>
					<div class="col-md-8">
						<div class="tab-contents">
							<div id="itemOne" class="tab-content-item active">
								<img src="./img/Item_detail_1.jpg" alt="">
							</div>
							<div id="itemTwo" class="tab-content-item">
								<img src="./img/Item_detail_2.jpg" alt="">
							</div>
							<div id="itemThree" class="tab-content-item">
								<img src="./img/Item_detail_3.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function() {
			$('.tab-products > li > a').on('click', function() {
				var target = $(this).attr('href');
				$('.tab-products > li').removeClass('active');
				$(this).parent().addClass('active');

				$('.tab-content-item').removeClass('active');
				$(target).addClass('active');
				return false;
			});
		});
	</script>
</body>
</html>