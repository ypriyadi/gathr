<!DOCTYPE html>
<html>
<head>
	<title>Gathr - Connected faster and simply</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<style type="text/css">
		body {
			background-color: #F1F4F5;
		}
		#login {
			margin-top: 60px;
		}
		.form-wrap {
			width: 400px;
			margin-left: auto;
			margin-right: auto;
			padding: 30px 45px 45px;
			background-color: #FFFFFF;
		}
		h1 {
			font-size: 24px;
			text-align: center;
			margin-bottom: 30px;
		}
		.btn-login {
			width: 100%;
		}
	</style>
</head>
<body>

	<section id="login">
	    <div class="container">
    	    <div class="form-wrap">
            	<h1>LOGIN</h1>
                <form role="form" action="javascript:;" method="post" id="login-form" autocomplete="off">
                    <div class="form-group">
                        <label for="email" class="sr-only">Email</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Your email">
                    </div>
                    <div class="form-group">
                        <label for="key" class="sr-only">Password</label>
                        <input type="password" name="key" id="key" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="key" class="sr-only">Password confirmation</label>
                        <input type="password" name="key" id="key" class="form-control" placeholder="Password confirmation">
                    </div>
                    <input type="submit" id="btn-login" class="btn btn-primary btn-login" value="Register">
                </form>
                <hr>
                <p>Already have an account? <a href="./login.php">Login now.</a></p>
    	    </div>
	    </div> <!-- /.container -->
	</section>
</body>
</html>